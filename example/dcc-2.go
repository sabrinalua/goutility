package main

import (
  "fmt"
)

/*
Given an array of integers, return a new array such that
each element at index i of the new array is the product of all the numbers
 in the original array except the one at i.

For example,
if our input was [1, 2, 3, 4, 5],
the expected output would be [120, 60, 40, 30, 24].
If our input was [3, 2, 1], the expected output would be [2, 3, 6].
*/

func product_of_all_but_needle(array []int) (total []int) {
  for i := 0; i < len(array); i++ {
    before:= array[:i]
    after:= array[i:]

    after=after[1:]
    t:=1
    for i := 0; i < len(before); i++ {
      t=t*before[i]

    }
    for j := 0; j < len(after); j++ {
      t=t*after[j]
    }
    total= append(total,t)
  }
  fmt.Println("total", total)
  return
}
